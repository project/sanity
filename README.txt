
Sanity Module
=============

This module is used for checking some basic settings in your Drupal 
installation and detecting settings that might suck up excessive CPU time 
during a cron run.

I was inspired to write this after my web provider brought it to my attention
that my Drupal installation was using a lot of CPU time.


Author
======

Douglas T. Muth - http://www.claws-and-paws.com/


Credits
=======

My web provider, nearlyfreespeech.net, for providing me with lots of detailed
information on how my Drupal installation was misbehaving so that I could
fix the problem.



