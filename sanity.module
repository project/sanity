<?php
/**
* This module does some basic sanity checking on a Drupal installation, 
* which is helpful to keep from getting in trouble with whoever owns the
* machine that you set it up on. :-)
*
* Copyright 2006, Douglas T. Muth - http://www.claws-and-paws.com/
* Use is granted under the GNU Public License
*
* Module version: 0.1
* Requirements: Drupal 4.6.5
*
*/


/**
 * Implementation of hook_help().
 */
function sanity_help($section) {
  switch ($section) {
    case 'admin/modules#description':
      return t('Performs basic sanity checks on your Drupal installation.');
  }
}


function sanity_perm() {  
	return(array('view sanity'));
}


/**
* Display the menu items for this module.
*
*/
function sanity_menu($may_cache) {

	$retval = array();

	/**
	* @todo I /still/ do not fully understand how $may_cache is supposed to
	* work.  I need to research this further in the future.
	*/
	$may_cache=1;

	if ($may_cache) {
		$menu_type = MENU_CALLBACK |MENU_VISIBLE_IN_TREE 
			|MENU_MODIFIABLE_BY_ADMIN;

		$retval[] = array(
			"path" => "admin/sanity",
			"title" => t("sanity check"),
			"access" => user_access('view sanity'),
			"callback" => "sanity_check",
			"type" => $menu_type,
			);
	}

	return($retval);

} // End of sanity_menu()


/**
* This function is our callback, which in turn runs our sanity checks 
* on the current Drupal installlation.
*
*/
function sanity_check() {

	$content = "";

	sanity_check_nodes($content);
	sanity_check_search($content);
	sanity_check_poormanscron($content);

	//
	// Send our content to the user
	// 
	print theme("page", $content); 

} // End of sanity_check()


/**
* Check all of our nodes and make sure that they are not too large.
*
* @param string $content Any content to send back to the user is appended
*	to this string.
*/
function sanity_check_nodes(&$content) {

	$max_k = 50;
	//$max_k = 5000; // For debugging

	//
	// Select our offending nodes, and order them in reverse order of
	// size so that the "low-hanging fruit" can be picked first.
	//
	$query = "SELECT nid, length(body) AS node_len, title "
		. "FROM node "
		. "WHERE length(body) > (1024 * $max_k) "
		. "ORDER BY node_len DESC ";

	$cursor = db_query($query);

	$content .= "<h2>" . t("Nodes over %max_k K, biggest listed first", 
		array("%max_k" => $max_k)) 
		. "</h2>\n";

	$beenhere = 0;
	
	//
	// Loop through our results.  If we find anything, start printing up 
	// a list of links for each offending now.
	//
	while ($result = db_fetch_array($cursor)) {
	
		if (!$beenhere) {
			$content .= "<ul>\n";
			$beenhere = 1;
		}

		$nid = $result['nid'];
		$title = $result['title'];
		$node_len = $result['node_len'];
		$node_len = ceil($node_len / 1024);
		$content .= "<li><a href=\"/node/$nid\">$title</a> ($node_len K)</li>";

	}

	//
	// Close off our list, or tell the user that we didn't find any nodes.
	//
	if ($beenhere) {
		$content .= "</ul>";
		$content .= t("One more more nodes over %max_k K were found.  it is recommended that you split these up into smaller nodes using the book module so as to decrease the time it takes to index each one of them.<p>", 
		array("%max_k" => $max_k));

	} 
	else {
		$content .= t("No nodes found over %max_k K<p>", 
			array("%max_k" => $max_k));
	}

} // End of sanity_check_nodes()


/**
* Make sure that we are not searching through too many nodes per cron run.
*
* @param string $content Any content to send back to the user is appended
*	to this string.
*/
function sanity_check_search(&$content) {

	$search_cron_max = 10;
	//$search_cron_max = 1; // For debugging
	$search_cron = variable_get("search_cron_limit", null);

	$content .= "<h2>" . t("Search settings") . "</h2>";

	if ($search_cron) {
		if ($search_cron > $search_cron_max) {
			$content .= t("<b>Warning</b>: More than %max nodes are being indexed per cron run!  This can cause excessive CPU usage for each cron run.<p>", 
				array("%max" => $search_cron_max));

		} 
		else {
			$content .= t("%max nodes or less are being indexed per cron run.  This is OK.<p>", array("%max" => $search_cron_max));

		}

	} 
	else {
		$content .= t("The search module does not appear to be in use.  This is OK.<p>");

	}

} // End of sanity_check_search()


/**
* Make sure that we are not calling poormanscron too frequently.
*
* @param string $content Any content to send back to the user is appended
*	to this string.
*/
function sanity_check_poormanscron(&$content) {

	$interval = variable_get("poormanscron_interval", null);
	$interval_max = 10;
	//$interval_max = 1000; // For debugging

	$content .= "<h2>" . t("Poormanscron settings") . "</h2>";

	if ($interval) {
		if ($interval < $interval_max) {
			$content .= t("<b>Warning</b>: Poormanscron is running crontabs more frequently than once every %max minutes.  This may cause excessive CPU load!", 
				array("%max" => $interval_max));

		} 
		else {
			$content .= t("Poormanscron is running crontabs less frequently than once every %max minutes.  This is OK.<p>", 
				array("%max" => $interval_max));

			$content .= t("Be advised, however, that if a crontab is scheduled so infrequently that it takes TOO long to run, the process may get killed which can lead to other problems.  If you have long running crontabs, you may want to investigate which module is taking so long by installing <a href=\"%url\">this patch</a>.", 
				array("%url" =>
					"http://www.claws-and-paws.com/software/drupal-patches")
				);

		}

	} 
	else {
		$content .= t("Poormanscron does not appear to be in use.  This is OK.<p>");

	}

} // End of sanity_check_poormanscron()


?>
